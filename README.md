# Void-KDE

Setup of KDE on Voidlinux

In my search for a great KDE spin I landed on Void Linux. There is a very active group of devs
in the Void community, that keep KDE very much up to date (we are talking Arch/KDE Neon-like speed ;-).

Second to that, the kde5 and kde5-baseapps provide the absolute bare KDE experience, without all K-fluff. For installing that it is sufficient to follow the guide at https://docs.voidlinux.org/config/graphical-session/kde.html.

In general I find the Void repos to be quite good for packages, but with some minor additions on my part. I added Joplin through their wget instructions on the web. I've been using Joplin for over 8 years now, so i trust to run their script:

   wget -O - https://raw.githubusercontent.com/laurent22/joplin/dev/Joplin_install_and_update.sh | bash

Other applications (Brave, Handbrake, Hypnotix) I have installed by using Distrobox. I will put up a different readme on how-to install that on VoidLinux. Not complicated at all.

   Void-wise I started with the basic live iso and installed the system with no swap. From there I installed/added the following things including zram, the latest kernel, pipewire and the repo for unfree software:

    1. Update all package system
       sudo xbps-install -Suv

    2. add non-free repo
       sudo xbps-install -Rs void-repo-nonfree

    3. sudo xbps-install mc micro linux-mainline neofetch xdg-user-dirs htop opendoas git zramen xdg-desktop-portal xdg-desktop-portal-gtk xdg-user-dirs xdg-user-dirs-gtk xdg-utils pipewire libspa-bluetooth ntfs-3g wget udisks2 gvfs mtpfs gvfs-mtp gvfs-gphoto2 xtools

    4. enable zram: sudo ln -s /etc/sv/zramen /var/service

    5. install kde5 & kde5-baseapps

The rest is of course up to you ;-)
