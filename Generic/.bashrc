# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls="lsd -a -l --color always --icon always --group-directories-first"
alias syu="doas xbps-install -Su  && flatpak upgrade"
alias mcb="doas mc -b"
PS1='[\u@\h \W]\$ '
